package com.action.ecommerceapp.data.network;

import com.action.ecommerceapp.data.network.responses.CategoriesResponse;
import com.action.ecommerceapp.data.network.responses.ProductsResponse;
import com.action.ecommerceapp.data.network.responses.VendorsResponse;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * {@link ServerApi} an interface with endpoints
 */
public interface ServerApi {

    @GET("categories")
    Call<CategoriesResponse> getCategories();

    @GET("products")
    Call<ProductsResponse> getProducts();

    @GET("vendors")
    Call<VendorsResponse> getVendors();
}
