package com.action.ecommerceapp.data.network.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Category implements Serializable {

    private final static long serialVersionUID = -807454153586208629L;
    @SerializedName("id")
    @Expose
    private long id;
    @SerializedName("parent_id")
    @Expose
    private Object parentId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("sort")
    @Expose
    private long sort;
    @SerializedName("checked")
    @Expose
    private boolean checked;
    @SerializedName("children")
    @Expose
    private List<Child> children = null;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Object getParentId() {
        return parentId;
    }

    public void setParentId(Object parentId) {
        this.parentId = parentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getSort() {
        return sort;
    }

    public void setSort(long sort) {
        this.sort = sort;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public List<Child> getChildren() {
        return children;
    }

    public void setChildren(List<Child> children) {
        this.children = children;
    }

}
