package com.action.ecommerceapp.data.network.responses;

import com.action.ecommerceapp.data.network.models.Category;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class CategoriesResponse implements Serializable {

    private final static long serialVersionUID = 4218805557638785033L;
    @SerializedName("categories")
    @Expose
    private List<Category> categories = null;

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(final List<Category> categories) {
        this.categories = categories;
    }

}
