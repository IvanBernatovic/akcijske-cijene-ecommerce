package com.action.ecommerceapp.domain.interactors;


import android.support.annotation.Nullable;

import com.action.ecommerceapp.data.network.RetrofitNetManager;
import com.action.ecommerceapp.data.network.models.Category;
import com.action.ecommerceapp.data.network.models.Vendor;
import com.action.ecommerceapp.data.network.responses.CategoriesResponse;
import com.action.ecommerceapp.data.network.responses.VendorsResponse;
import com.action.ecommerceapp.presentation.model.FilterModel;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.BiFunction;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FiltersUseCase extends UseCase<FilterModel, FiltersUseCase.Params> {

    private final ObservableSource<List<Vendor>> vendorsObservable = new Observable<List<Vendor>>() {
        @Override
        protected void subscribeActual(@NonNull final Observer<? super List<Vendor>> observer) {
            RetrofitNetManager.getInstance()
                    .getServerApi()
                    .getVendors()
                    .enqueue(new Callback<VendorsResponse>() {
                        @Override
                        public void onResponse(@android.support.annotation.NonNull final Call<VendorsResponse> call,
                                               @android.support.annotation.NonNull final Response<VendorsResponse> response) {
                            if (response.isSuccessful() && response.body() != null && response.body().getVendors() != null) {
                                observer.onNext(response.body().getVendors());
                                observer.onComplete();
                            }
                        }

                        @Override
                        public void onFailure(@android.support.annotation.NonNull final Call<VendorsResponse> call,
                                              @android.support.annotation.NonNull final Throwable t) {
                            observer.onError(t);
                        }
                    });
        }
    };
    private final ObservableSource<List<Category>> categoriesObservable = new Observable<List<Category>>() {
        @Override
        protected void subscribeActual(@NonNull final Observer<? super List<Category>> observer) {
            RetrofitNetManager.getInstance()
                    .getServerApi()
                    .getCategories()
                    .enqueue(new Callback<CategoriesResponse>() {
                        @Override
                        public void onResponse(@android.support.annotation.NonNull final Call<CategoriesResponse> call,
                                               @android.support.annotation.NonNull final Response<CategoriesResponse> response) {
                            if (response.isSuccessful() && response.body() != null && response.body().getCategories() != null) {
                                observer.onNext(response.body().getCategories());
                                observer.onComplete();
                            }
                        }

                        @Override
                        public void onFailure(@android.support.annotation.NonNull final Call<CategoriesResponse> call,
                                              @android.support.annotation.NonNull final Throwable t) {
                            observer.onError(t);
                        }
                    });
        }
    };

    /**
     * In this UseCase we send two requests (for vendors and categories) simultaneously,
     * wait for the response and zip responses to the one model.
     */
    @Override
    protected Observable<FilterModel> buildObservable(@Nullable final Params params) {
        return Observable.zip(vendorsObservable, categoriesObservable, new BiFunction<List<Vendor>, List<Category>, FilterModel>() {
            @Override
            public FilterModel apply(@NonNull final List<Vendor> vendorList, final @NonNull List<Category> categoryList) throws Exception {
                return new FilterModel(vendorList, categoryList);
            }
        });
    }


    static class Params {}
}
