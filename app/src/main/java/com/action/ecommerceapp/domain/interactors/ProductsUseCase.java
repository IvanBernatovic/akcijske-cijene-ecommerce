package com.action.ecommerceapp.domain.interactors;


import android.support.annotation.Nullable;

import com.action.ecommerceapp.data.network.RetrofitNetManager;
import com.action.ecommerceapp.data.network.responses.ProductsResponse;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.annotations.NonNull;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductsUseCase extends UseCase<ProductsResponse, ProductsUseCase.Params> {


    @Override
    protected Observable<ProductsResponse> buildObservable(@Nullable Params params) {
        return Observable.create(new ObservableOnSubscribe<ProductsResponse>() {
            @Override
            public void subscribe(@NonNull final ObservableEmitter<ProductsResponse> emitter) throws Exception {
                RetrofitNetManager.getInstance()
                        .getServerApi()
                        .getProducts().enqueue(new Callback<ProductsResponse>() {
                    @Override
                    public void onResponse(Call<ProductsResponse> call, Response<ProductsResponse> response) {
                        if (response.isSuccessful()) {
                            emitter.onNext(response.body());
                        }
                        emitter.onComplete();
                    }

                    @Override
                    public void onFailure(Call<ProductsResponse> call, Throwable error) {
                        emitter.onError(error);
                    }
                });
            }
        });
    }

    static class Params {
    }
}
