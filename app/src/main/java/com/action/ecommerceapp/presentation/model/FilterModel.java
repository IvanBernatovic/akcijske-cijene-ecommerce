package com.action.ecommerceapp.presentation.model;


import android.support.annotation.NonNull;

import com.action.ecommerceapp.data.network.models.Category;
import com.action.ecommerceapp.data.network.models.Vendor;

import java.io.Serializable;
import java.util.List;

public class FilterModel implements Serializable {
    private List<Vendor> vendorList;
    private List<Category> categoryList;
    private String maxPrice;

    public FilterModel(@NonNull final List<Vendor> vendorList, @NonNull final List<Category> categoryList) {
        this.vendorList = vendorList;
        this.categoryList = categoryList;
    }

    public List<Vendor> getVendorList() {
        return vendorList;
    }

    public void setVendorList(List<Vendor> vendorList) {
        this.vendorList = vendorList;
    }

    public List<Category> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(List<Category> categoryList) {
        this.categoryList = categoryList;
    }

    public String getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(String maxPrice) {
        this.maxPrice = maxPrice;
    }
}
