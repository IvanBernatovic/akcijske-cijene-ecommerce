package com.action.ecommerceapp.presentation.presenter;


/**
 * {@link Presenter} an interface that represents a lifecycle of the activity/fragment
 */
public interface Presenter {

    void onStop();

    void loadData();
}
