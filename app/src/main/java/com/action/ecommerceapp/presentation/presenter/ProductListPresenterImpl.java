package com.action.ecommerceapp.presentation.presenter;


import com.action.ecommerceapp.data.network.models.Product;
import com.action.ecommerceapp.data.network.responses.ProductsResponse;
import com.action.ecommerceapp.domain.interactors.ProductsUseCase;
import com.action.ecommerceapp.presentation.view.interfaces.IDataLoader;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.annotations.NonNull;
import io.reactivex.observers.DisposableObserver;

public class ProductListPresenterImpl implements Presenter {


    private final IDataLoader<List<Product>> view;
    private final ProductsUseCase useCase;
    private ArrayList<Product> productsList;

    public ProductListPresenterImpl(IDataLoader<java.util.List<com.action.ecommerceapp.data.network.models.Product>> view) {
        this.view = view;
        this.useCase = new ProductsUseCase();
    }

    @Override
    public void onStop() {
        this.useCase.dispose();
    }

    @Override
    public void loadData() {
        this.view.showLoading();
        if (productsList == null) {
            this.useCase.execute(new ProductObserver(), null);
        } else {
            this.view.hideLoading();
            this.view.onDataLoaded(productsList);
        }
    }

    private class ProductObserver extends DisposableObserver<ProductsResponse> {

        @Override
        public void onNext(@NonNull ProductsResponse productsResponse) {
            view.onDataLoaded(productsResponse.getProducts());
            productsList = new ArrayList<>(productsResponse.getProducts());
        }

        @Override
        public void onError(@NonNull Throwable e) {
            view.hideLoading();
        }

        @Override
        public void onComplete() {
            view.hideLoading();
        }
    }
}
