package com.action.ecommerceapp.presentation.presenter;


import com.action.ecommerceapp.presentation.model.FilterModel;

import io.reactivex.annotations.NonNull;

/**
 * {@link ProductsFilterPresenter} an interface for interactions between View (Activity or Fragment)
 * and Presenter
 */
public interface ProductsFilterPresenter extends Presenter {

    void onCategoryClicked(int groupPosition, int childPosition, boolean isChecked);

    void onVendorChecked(int position, boolean isChecked);

    FilterModel getFilters();

    void setFilters(@NonNull FilterModel filters);
}
