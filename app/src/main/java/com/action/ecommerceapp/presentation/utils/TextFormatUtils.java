package com.action.ecommerceapp.presentation.utils;


import android.content.Context;
import android.support.annotation.NonNull;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.StrikethroughSpan;

import com.action.ecommerceapp.R;
import com.action.ecommerceapp.data.network.models.Product;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * {@link TextFormatUtils} used for text formatting
 */
public class TextFormatUtils {
    private static final String CURRENCY_NAME = " kn ";

    /**
     * A method that formats an old and a new product prices
     *
     * @param context context
     * @param product {@link Product} to format
     * @return formatted spannable string
     */
    public static SpannableString getFormattedProductPrice(@NonNull final Context context, @NonNull final Product product) {

        final String s = product.getNewPrice() + CURRENCY_NAME + product.getOldPrice() + CURRENCY_NAME;
        final SpannableString spannableString = new SpannableString(s);

        final ForegroundColorSpan colorSpan = new ForegroundColorSpan(context.getResources().getColor(R.color.colorOrange));
        spannableString.setSpan(colorSpan, 0, product.getNewPrice().length() + CURRENCY_NAME.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);

        final StrikethroughSpan strikethroughSpan = new StrikethroughSpan();
        spannableString.setSpan(strikethroughSpan, s.length() - product.getOldPrice().length() - CURRENCY_NAME.length(), s.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        return spannableString;
    }

    /**
     * A method that formats a product date
     *
     * @param context context
     * @param product {@link Product} to format
     * @return formatted spannable string
     */
    public static String getFormattedProductDate(@NonNull final Context context, @NonNull final Product product) {
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM", Locale.ENGLISH);
        final SimpleDateFormat originalDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);

        Date activeFrom = null;
        Date activeTo = null;
        try {
            activeFrom = originalDate.parse(product.getActiveFrom());
            activeTo = originalDate.parse(product.getActiveTo());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        final String date = context.getString(R.string.product_date);

        return String.format(date, simpleDateFormat.format(activeFrom),
                simpleDateFormat.format(activeTo));
    }
}
