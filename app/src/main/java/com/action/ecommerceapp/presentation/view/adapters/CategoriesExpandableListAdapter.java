package com.action.ecommerceapp.presentation.view.adapters;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.action.ecommerceapp.R;
import com.action.ecommerceapp.data.network.models.Category;
import com.action.ecommerceapp.data.network.models.Child;

import java.util.ArrayList;
import java.util.List;

public class CategoriesExpandableListAdapter extends BaseExpandableListAdapter {

    private final OnCategoryCheckedListener onCategoryCheckedListener;
    private List<Category> categoryList;

    public CategoriesExpandableListAdapter(OnCategoryCheckedListener onCategoryCheckedListener) {
        this.onCategoryCheckedListener = onCategoryCheckedListener;
    }

    @Override
    public int getGroupCount() {
        return categoryList == null ? 0 : categoryList.size();
    }

    @Override
    public int getChildrenCount(final int position) {
        return categoryList == null ? 0 : categoryList.get(position).getChildren().size();
    }

    @Override
    public Object getGroup(final int position) {
        return categoryList == null ? 0 : categoryList.get(position);
    }

    @Override
    public Object getChild(final int listPosition, final int expandedListPosition) {
        return categoryList == null ? 0 : categoryList.get(listPosition).getChildren().get(expandedListPosition);
    }

    @Override
    public long getGroupId(final int position) {
        return categoryList == null ? 0 : categoryList.get(position).getId();
    }

    @Override
    public long getChildId(final int listPosition, final int expandedListPosition) {
        return categoryList == null ? 0 : categoryList.get(listPosition).getChildren().get(expandedListPosition).getId();
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(final int listPosition, final boolean isExpanded,
                             @Nullable View convertView, @NonNull final ViewGroup parent) {
        final Category category = (Category) getGroup(listPosition);
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) parent.getContext().
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.list_group, null);
        }
        TextView groupTitle = convertView.findViewById(R.id.tvGroupName);
        groupTitle.setText(category.getName());
        return convertView;
    }

    @Override
    public View getChildView(final int listPosition, final int expandedListPosition,
                             final boolean isLastChild, @Nullable View convertView,
                             @NonNull final ViewGroup parent) {
        final Child child = (Child) getChild(listPosition, expandedListPosition);
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) parent.getContext().
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.list_child, null);
        }
        final CheckBox checkBox = convertView.findViewById(R.id.chbCategory);
        checkBox.setText(child.getName());
        checkBox.setChecked(child.isChecked());
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                categoryList.get(listPosition).getChildren().get(expandedListPosition).setChecked(checkBox.isChecked());
                onCategoryCheckedListener.onCategoryChecked(listPosition, expandedListPosition, checkBox.isChecked());
            }
        });
        return convertView;
    }

    @Override
    public boolean isChildSelectable(final int listPosition, final int expandedListPosition) {
        return true;
    }

    public void setData(List<Category> data) {
        this.categoryList = new ArrayList<>(data);
    }

    public interface OnCategoryCheckedListener {
        void onCategoryChecked(int listPosition, int expandedListPosition, boolean isChecked);
    }
}
