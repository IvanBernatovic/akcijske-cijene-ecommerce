package com.action.ecommerceapp.presentation.view.adapters;


import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import com.action.ecommerceapp.R;
import com.action.ecommerceapp.data.network.models.Vendor;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class VendorsListAdapter extends RecyclerView.Adapter<VendorsListAdapter.ItemViewHolder> {

    private final ItemViewHolder.OnItemCheckListener onItemCheckListener;
    private ArrayList<Vendor> data;

    public VendorsListAdapter(@NonNull final ItemViewHolder.OnItemCheckListener onItemCheckListener) {
        this.onItemCheckListener = onItemCheckListener;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull final ViewGroup parent, final int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_vendor, parent, false);
        return new ItemViewHolder(view, onItemCheckListener);
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        final Vendor vendor = data.get(position);
        holder.cbVendorName.setText(vendor.getName());
        holder.cbVendorName.setChecked(vendor.isChecked());
    }

    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();
    }

    public void setData(@NonNull final List<Vendor> vendorList) {
        if (this.data == null) {
            this.data = new ArrayList<>(vendorList);
            notifyItemRangeInserted(0, data.size());
        } else {
            DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(new DiffUtil.Callback() {
                @Override
                public int getOldListSize() {
                    return data.size();
                }

                @Override
                public int getNewListSize() {
                    return vendorList.size();
                }

                @Override
                public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
                    return data.get(oldItemPosition).getId() == vendorList.get(newItemPosition).getId();
                }

                @Override
                public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
                    return Objects.equals(data.get(oldItemPosition).getName(), vendorList.get(newItemPosition).getName())
                            && data.get(oldItemPosition).getSort() == vendorList.get(newItemPosition).getSort();
                }
            }, false);
            this.data = new ArrayList<>(vendorList);
            diffResult.dispatchUpdatesTo(this);
        }
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder {

        private final CheckBox cbVendorName;

        public ItemViewHolder(@NonNull final View itemView,
                              @NonNull final OnItemCheckListener onItemCheckListener) {
            super(itemView);
            this.cbVendorName = itemView.findViewById(R.id.cbVendorName);
            this.cbVendorName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onItemCheckListener.onItemChecked(getAdapterPosition(), cbVendorName.isChecked());
                }
            });
        }


        public interface OnItemCheckListener {
            void onItemChecked(int position, boolean checked);
        }
    }
}
