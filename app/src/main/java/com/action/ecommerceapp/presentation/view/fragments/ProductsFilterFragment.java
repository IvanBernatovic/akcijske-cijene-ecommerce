package com.action.ecommerceapp.presentation.view.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ProgressBar;

import com.action.ecommerceapp.R;
import com.action.ecommerceapp.presentation.model.FilterModel;
import com.action.ecommerceapp.presentation.presenter.ProductsFilterPresenter;
import com.action.ecommerceapp.presentation.presenter.ProductsFilterPresenterImpl;
import com.action.ecommerceapp.presentation.view.adapters.CategoriesExpandableListAdapter;
import com.action.ecommerceapp.presentation.view.adapters.VendorsListAdapter;
import com.action.ecommerceapp.presentation.view.interfaces.IDataLoader;
import com.action.ecommerceapp.presentation.view.interfaces.IFilterCallback;

public class ProductsFilterFragment extends BaseFragment implements IDataLoader<FilterModel>,
        VendorsListAdapter.ItemViewHolder.OnItemCheckListener, View.OnClickListener,
        CategoriesExpandableListAdapter.OnCategoryCheckedListener {

    public static final String KEY_FILTERS_MODEL = "KEY_FILTERS_MODEL";
    private IFilterCallback iFilterCallback;
    private ProductsFilterPresenter presenter;
    private ProgressBar progressBar;
    private VendorsListAdapter vendorsAdapter;
    private CategoriesExpandableListAdapter expandableListAdapter;
    private FilterModel filtersModel;
    private EditText etMaxPrice;

    @Override
    public void onAttach(@NonNull final Context context) {
        super.onAttach(context);
        if (context instanceof IFilterCallback) {
            this.iFilterCallback = (IFilterCallback) context;
        }
    }

    @Override
    public void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.presenter = new ProductsFilterPresenterImpl(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater,
                             @Nullable final ViewGroup container,
                             @Nullable final Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_profucts_filter, container, false);
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getArguments() != null) {
            this.filtersModel = (FilterModel) getArguments().getSerializable(KEY_FILTERS_MODEL);
        }
        this.progressBar = view.findViewById(R.id.progressBar);
        final Button btnSave = view.findViewById(R.id.btnSave);
        btnSave.setOnClickListener(this);

        this.etMaxPrice = view.findViewById(R.id.etMaxPrice);

        final RecyclerView rvVendors = view.findViewById(R.id.rvVendors);
        rvVendors.setLayoutManager(new LinearLayoutManager(this.getContext(), LinearLayoutManager.VERTICAL, false));
        this.vendorsAdapter = new VendorsListAdapter(this);
        rvVendors.setAdapter(vendorsAdapter);

        expandableListAdapter = new CategoriesExpandableListAdapter(this);
        final ExpandableListView expandableListView = view.findViewById(R.id.elvCategory);
        expandableListView.setAdapter(expandableListAdapter);

        if (this.filtersModel != null) {
            this.presenter.setFilters(filtersModel);
            this.hideLoading();
            this.onDataLoaded(filtersModel);
            this.etMaxPrice.setText(filtersModel.getMaxPrice());
        } else {
            this.presenter.loadData();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        this.presenter.onStop();
    }

    @Override
    public void onDataLoaded(@NonNull final FilterModel data) {
        this.vendorsAdapter.setData(data.getVendorList());
        this.expandableListAdapter.setData(data.getCategoryList());
        this.expandableListAdapter.notifyDataSetChanged();
    }

    @Override
    public void hideLoading() {
        this.progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showLoading() {
        this.progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onItemChecked(final int position, final boolean isChecked) {
        this.presenter.onVendorChecked(position, isChecked);
    }

    @Override
    public void onClick(@NonNull final View view) {
        switch (view.getId()) {
            case R.id.btnSave:
                onBtnSaveClicked();
                break;
        }
    }

    private void onBtnSaveClicked() {
        final FilterModel filterModel = this.presenter.getFilters();
        filterModel.setMaxPrice(this.etMaxPrice.getText().toString());
        iFilterCallback.onFilterSelected(filterModel);
        getActivity().onBackPressed();
    }

    @Override
    public void onCategoryChecked(int listPosition, int expandedListPosition, boolean isChecked) {
        this.presenter.onCategoryClicked(listPosition, expandedListPosition, isChecked);
    }
}
